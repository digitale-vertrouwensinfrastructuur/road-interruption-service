# Build
FROM golang:1.16-alpine AS build

WORKDIR /go/src/app
ADD . /go/src/app/

RUN go mod download
RUN go build -o ./dist/bin/road-interruption-service ./cmd/road-interruption-service/main.go

# Release 
FROM alpine:latest

RUN apk update && apk add ca-certificates

COPY --from=build /go/src/app/dist/bin/road-interruption-service /usr/local/bin/road-interruption-service

RUN adduser -D -u 1001 appuser
USER appuser

EXPOSE 8080

CMD ["/usr/local/bin/road-interruption-service"]
