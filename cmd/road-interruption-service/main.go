package main

// Licensed under the MIT license
import (
	"fmt"
	"log"
	"net/http"

	"gitlab.com/digitale-vertrouwensinfrastructuur/road-interruption-service/app"
)

func main() {
	resp, err := http.Get("https://gobyexample.com")
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()

	fmt.Println("Response status:", resp.Status)

	app := app.App()
	log.Fatal(app.Listen(":8080"))
}
