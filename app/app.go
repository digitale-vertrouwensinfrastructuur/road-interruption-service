// Licensed under the MIT license
package app

import (
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/logger"
	"github.com/gofiber/fiber/v2/middleware/requestid"
)

func App() *fiber.App {
	app := fiber.New()

	app.Use(requestid.New())

	app.Use(logger.New(logger.Config{
		Format: "${pid} ${locals:requestid} ${status} - ${method} ${path}\n",
	}))

	app.Get("/health", func(c *fiber.Ctx) error {
		return c.SendString("healthy")
	})

	app.Get("/api/v1/road-interruptions", func(c *fiber.Ctx) error {
		var interruptions = []RoadInterruption{
			{Road: "Dagobertducklaan"}, {Road: "Guusgelukparkweg"}, {Road: "Katrienplein"}, {Road: "Kwekkade"}}
		return c.JSON(interruptions)
	})

	return app
}
