// Licensed under the MIT license
package app

import (
	"io/ioutil"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestIndexRoute(t *testing.T) {
	tests := []struct {
		description string

		route string

		expectedError bool
		expectedCode  int
		expectedBody  string
	}{
		{
			description:   "index route",
			route:         "/health",
			expectedError: false,
			expectedCode:  200,
			expectedBody:  "healthy",
		},
		{
			description:   "get all road interruptions",
			route:         "/api/v1/road-interruptions",
			expectedError: false,
			expectedCode:  200,
			expectedBody:  `[{"road":"Dagobertducklaan"},{"road":"Guusgelukparkweg"},{"road":"Katrienplein"},{"road":"Kwekkade"}]`,
		},
	}

	app := App()

	for _, test := range tests {
		req, _ := http.NewRequest(
			"GET",
			test.route,
			nil,
		)

		res, err := app.Test(req, -1)

		assert.Equalf(t, test.expectedError, err != nil, test.description)

		if test.expectedError {
			continue
		}
		assert.Equalf(t, test.expectedCode, res.StatusCode, test.description)

		body, err := ioutil.ReadAll(res.Body)

		assert.Nilf(t, err, test.description)

		assert.Equalf(t, test.expectedBody, string(body), test.description)
	}
}
