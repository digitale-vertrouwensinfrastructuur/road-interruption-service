# road-interruption-service
The road interruption service is responsible for providing information about roads that cannot be used due to roadworks or other interruptions.

## Requirements
- go 1.16+

## Development with hot reload

### Install modd and run
```
modd
```

## Building

```
go build -o dist/bin/road-interruption-service ./cmd/road-interruption-service/main.go
```

## Running locally
```
go run ./cmd/road-interruption-service/main.go
```

### Run on kubernetes using Skaffold
Install microk8s according to [Microk8s installation](https://microk8s.io/docs/install-alternatives)
Install Helm according to [Helm installation](https://helm.sh/docs/intro/install/)
Install Skaffold according to [Skaffold installation](https://skaffold.dev/docs/install/)
Copy the values from `microk8s.kubectl config view` to $HOME/.kube/config

```bash
docker login registry.gitlab.com # a gitlab access token can be used as password here

kubectl config use-context microk8s
kubectl create ns dvi-municipality-amsterdam
kubectl create secret generic regcred \
    --from-file=.dockerconfigjson=</absolute/path/to/.docker/config.json> \
    --type=kubernetes.io/dockerconfigjson \
    --namespace=dvi-municipality-amsterdam
```
This `regcred` secret will be used by the Helm deployment to fetch the Docker image. Make sure to create it in the correct namespace.

Alternatively, it is also possible to create the regcred using:
```
kubectl create secret docker-registry regcred --docker-server=registry.gitlab.com --docker-username=<username> --docker-password=<gitlab-access-token> --docker-email=<email> --namespace=dvi-municipality-amsterdam
```

Then run:

```bash
skaffold dev --cleanup=false -p municipality-amsterdam
```

It may take some time for all resources to become healthy.

To test that the home-api is up, make sure you can connect to it, for example using a port-forward

```bash
kubectl port-forward service/road-interruption-service 8080:http --namespace=dvi-municipality-amsterdam
curl localhost:8080/health
```

### Run using Docker-compose
```
docker-compose up
```
## Testing

```
go test ./...
```

### Testing REST endpoints manually
The `examples/road-interruption-service.http` file can be used to test the REST endpoints

## License
See [LICENSE.md](LICENSE.md)
